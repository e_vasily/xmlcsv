using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using LiquidTechnologies.Viewer.Net40;
using System.Xml.Linq;
using LiquidTechnologies.Runtime.Net40;
using System.Collections.Generic;
using SampleApp;
using CsvHelper;
using CsvHelper.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace LiquidTechnologies.Test
{
	/// <summary>
	/// Summary description for SampleApp.
	/// </summary>
	public class SampleApp : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.ListBox lstFiles;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
        private Button button1;
        private TextBox textBox1;
        private Button button2;
        private Button button3;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

		#region btnLoad_Click - Opens the selected file - (THIS IS A GOOD PLACE TO START)
		// When an item is selected from the ListBox, this event handler is called.
		// 
		// If you specified Sample files in the Liquid XML wizard then they will be listed
		// in the switch statement.
		// If you did not specify Samples then there are 2 routes foward. You can either
		// use the "Browse for a File to Load", this will load the sample using a generic load
		// method. Alternatively you can run one of the SimpleTestXXXXX("<filename>") methods
		// This will load the file into the correct object, and display it in the viewer.
		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;

			switch (lstFiles.SelectedIndex)
			{
				case 0:
					if (openFileDialog.ShowDialog(this) == DialogResult.OK)
						SimpleTestLoadAnyXmlDocument(openFileDialog.FileName);
				break;
			};
			#region UNCOMMENT CODE IN THIS REGION TO LOAD YOUR SAMPLE XML FILES
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'assignment_info' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <assignment_info>
			 *     ...
			 * </assignment_info>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52Assignment_info(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'BirthCertificate' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <BirthCertificate>
			 *     ...
			 * </BirthCertificate>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns54BirthCertificate(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'BirthDate' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <BirthDate>
			 *     ...
			 * </BirthDate>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns53BirthDate(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'categoryID' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <categoryID>
			 *     ...
			 * </categoryID>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52CategoryID(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'data' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <data>
			 *     ...
			 * </data>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns55Data(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'dateFinish' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <dateFinish>
			 *     ...
			 * </dateFinish>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52DateFinish(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'dateStart' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <dateStart>
			 *     ...
			 * </dateStart>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52DateStart(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'decision_date' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <decision_date>
			 *     ...
			 * </decision_date>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52Decision_date(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'FamilyName' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <FamilyName>
			 *     ...
			 * </FamilyName>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns33FamilyName(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'FirstName' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <FirstName>
			 *     ...
			 * </FirstName>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns33FirstName(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'Gender' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <Gender>
			 *     ...
			 * </Gender>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns53Gender(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'ID' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <ID>
			 *     ...
			 * </ID>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52ID(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'IdentityDoc' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <IdentityDoc>
			 *     ...
			 * </IdentityDoc>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns53IdentityDoc(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'LMSZID' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <LMSZID>
			 *     ...
			 * </LMSZID>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52LMSZID(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'MSZ_receiver' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <MSZ_receiver>
			 *     ...
			 * </MSZ_receiver>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52MSZ_receiver(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'needsCriteria' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <needsCriteria>
			 *     ...
			 * </needsCriteria>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52NeedsCriteria(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'OSZCode' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <OSZCode>
			 *     ...
			 * </OSZCode>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52OSZCode(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'package' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <package>
			 *     ...
			 * </package>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns51Package(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'PassportRF' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <PassportRF>
			 *     ...
			 * </PassportRF>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns54PassportRF(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'Patronymic' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <Patronymic>
			 *     ...
			 * </Patronymic>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns33Patronymic(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'prsnInfo' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <prsnInfo>
			 *     ...
			 * </prsnInfo>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns53PrsnInfo(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'reason_persons' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <reason_persons>
			 *     ...
			 * </reason_persons>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns52Reason_persons(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			/* ---------------------------------------------------------------------------------
			 * This function can be used to open an XML document with a document element 
			 * (the first element in the file) named 'SNILS' 
			 * e.g. 
			 *
			 * <?xml version="1.0" encoding="UTF-8"?>
			 * <SNILS>
			 *     ...
			 * </SNILS>
			 * --------------------------------------------------------------------------------- */
			// SimpleTestns53SNILS(@"<UNCOMMENT & INSERT A SAMPLE XML FILENAME HERE>");
			
			#endregion
			this.Cursor = Cursors.Default;
		}
		#endregion
	
		#region Sample Functions Demonstrating Reading from an XML File
		#region SimpleTestns52Assignment_info - Demo for documents with a root <assignment_info>
		// Shows a simple example of how the class Assignment_info
		// can be used. This class can be used to load documents whose 
		// root (document) element is <assignment_info>.
		private void SimpleTestns52Assignment_info(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.Assignment_info elm = new ns52.Assignment_info();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <assignment_info>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns54BirthCertificate - Demo for documents with a root <BirthCertificate>
		// Shows a simple example of how the class BirthCertificate
		// can be used. This class can be used to load documents whose 
		// root (document) element is <BirthCertificate>.
		private void SimpleTestns54BirthCertificate(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns54.BirthCertificate elm = new ns54.BirthCertificate();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <BirthCertificate>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns53BirthDate - Demo for documents with a root <BirthDate>
		// Shows a simple example of how the class BirthDate
		// can be used. This class can be used to load documents whose 
		// root (document) element is <BirthDate>.
		private void SimpleTestns53BirthDate(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns53.BirthDate elm = new ns53.BirthDate();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <BirthDate>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52CategoryID - Demo for documents with a root <categoryID>
		// Shows a simple example of how the class CategoryID
		// can be used. This class can be used to load documents whose 
		// root (document) element is <categoryID>.
		private void SimpleTestns52CategoryID(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.CategoryID elm = new ns52.CategoryID();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <categoryID>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns55Data - Demo for documents with a root <data>
		// Shows a simple example of how the class Data
		// can be used. This class can be used to load documents whose 
		// root (document) element is <data>.
		private void SimpleTestns55Data(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns55.Data elm = new ns55.Data();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <data>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52DateFinish - Demo for documents with a root <dateFinish>
		// Shows a simple example of how the class DateFinish
		// can be used. This class can be used to load documents whose 
		// root (document) element is <dateFinish>.
		private void SimpleTestns52DateFinish(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.DateFinish elm = new ns52.DateFinish();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <dateFinish>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52DateStart - Demo for documents with a root <dateStart>
		// Shows a simple example of how the class DateStart
		// can be used. This class can be used to load documents whose 
		// root (document) element is <dateStart>.
		private void SimpleTestns52DateStart(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.DateStart elm = new ns52.DateStart();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <dateStart>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52Decision_date - Demo for documents with a root <decision_date>
		// Shows a simple example of how the class Decision_date
		// can be used. This class can be used to load documents whose 
		// root (document) element is <decision_date>.
		private void SimpleTestns52Decision_date(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.Decision_date elm = new ns52.Decision_date();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <decision_date>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns33FamilyName - Demo for documents with a root <FamilyName>
		// Shows a simple example of how the class FamilyName
		// can be used. This class can be used to load documents whose 
		// root (document) element is <FamilyName>.
		private void SimpleTestns33FamilyName(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns33.FamilyName elm = new ns33.FamilyName();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <FamilyName>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns33FirstName - Demo for documents with a root <FirstName>
		// Shows a simple example of how the class FirstName
		// can be used. This class can be used to load documents whose 
		// root (document) element is <FirstName>.
		private void SimpleTestns33FirstName(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns33.FirstName elm = new ns33.FirstName();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <FirstName>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns53Gender - Demo for documents with a root <Gender>
		// Shows a simple example of how the class Gender
		// can be used. This class can be used to load documents whose 
		// root (document) element is <Gender>.
		private void SimpleTestns53Gender(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns53.Gender elm = new ns53.Gender();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <Gender>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52ID - Demo for documents with a root <ID>
		// Shows a simple example of how the class ID
		// can be used. This class can be used to load documents whose 
		// root (document) element is <ID>.
		private void SimpleTestns52ID(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.ID elm = new ns52.ID();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <ID>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns53IdentityDoc - Demo for documents with a root <IdentityDoc>
		// Shows a simple example of how the class IdentityDoc
		// can be used. This class can be used to load documents whose 
		// root (document) element is <IdentityDoc>.
		private void SimpleTestns53IdentityDoc(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns53.IdentityDoc elm = new ns53.IdentityDoc();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <IdentityDoc>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52LMSZID - Demo for documents with a root <LMSZID>
		// Shows a simple example of how the class LMSZID
		// can be used. This class can be used to load documents whose 
		// root (document) element is <LMSZID>.
		private void SimpleTestns52LMSZID(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.LMSZID elm = new ns52.LMSZID();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <LMSZID>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52MSZ_receiver - Demo for documents with a root <MSZ_receiver>
		// Shows a simple example of how the class MSZ_receiver
		// can be used. This class can be used to load documents whose 
		// root (document) element is <MSZ_receiver>.
		private void SimpleTestns52MSZ_receiver(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.MSZ_receiver elm = new ns52.MSZ_receiver();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <MSZ_receiver>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52NeedsCriteria - Demo for documents with a root <needsCriteria>
		// Shows a simple example of how the class NeedsCriteria
		// can be used. This class can be used to load documents whose 
		// root (document) element is <needsCriteria>.
		private void SimpleTestns52NeedsCriteria(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.NeedsCriteria elm = new ns52.NeedsCriteria();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <needsCriteria>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52OSZCode - Demo for documents with a root <OSZCode>
		// Shows a simple example of how the class OSZCode
		// can be used. This class can be used to load documents whose 
		// root (document) element is <OSZCode>.
		private void SimpleTestns52OSZCode(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.OSZCode elm = new ns52.OSZCode();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <OSZCode>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns51Package - Demo for documents with a root <package>
		// Shows a simple example of how the class Package
		// can be used. This class can be used to load documents whose 
		// root (document) element is <package>.
		private void SimpleTestns51Package(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns51.Package elm = new ns51.Package();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <package>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns54PassportRF - Demo for documents with a root <PassportRF>
		// Shows a simple example of how the class PassportRF
		// can be used. This class can be used to load documents whose 
		// root (document) element is <PassportRF>.
		private void SimpleTestns54PassportRF(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns54.PassportRF elm = new ns54.PassportRF();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <PassportRF>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns33Patronymic - Demo for documents with a root <Patronymic>
		// Shows a simple example of how the class Patronymic
		// can be used. This class can be used to load documents whose 
		// root (document) element is <Patronymic>.
		private void SimpleTestns33Patronymic(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns33.Patronymic elm = new ns33.Patronymic();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <Patronymic>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns53PrsnInfo - Demo for documents with a root <prsnInfo>
		// Shows a simple example of how the class PrsnInfo
		// can be used. This class can be used to load documents whose 
		// root (document) element is <prsnInfo>.
		private void SimpleTestns53PrsnInfo(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns53.PrsnInfo elm = new ns53.PrsnInfo();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <prsnInfo>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns52Reason_persons - Demo for documents with a root <reason_persons>
		// Shows a simple example of how the class Reason_persons
		// can be used. This class can be used to load documents whose 
		// root (document) element is <reason_persons>.
		private void SimpleTestns52Reason_persons(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns52.Reason_persons elm = new ns52.Reason_persons();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <reason_persons>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#region SimpleTestns53SNILS - Demo for documents with a root <SNILS>
		// Shows a simple example of how the class SNILS
		// can be used. This class can be used to load documents whose 
		// root (document) element is <SNILS>.
		private void SimpleTestns53SNILS(string filename)
		{
			try
			{
				// create an instance of the class to load the XML file into
				ns53.SNILS elm = new ns53.SNILS();
				
				// load the xml from a file into the object (the root element in the
				// xml document must be <SNILS>.
				elm.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion

		#region SimpleTestLoadAnyXmlDocument
		// Shows a generic way to load up an XML document.
		private void SimpleTestLoadAnyXmlDocument(string filename)
		{
			try
			{
				// load the xml from a file into the object (the root element in the
				// xml document must be <ElmBaseElement>.
				LiquidTechnologies.Runtime.Net40.XmlObjectBase elm = schema0Lib.ClassFactory.FromXmlFile(filename);

				// This will open up a viewer, allowing you to navigate the classes
				// that have been generated. 
				// Note the veiwer can be used to modify properties, and provides a listing of
				// the code required to create the document it is displaying.
				SimpleViewer sv = new SimpleViewer(elm);
				sv.ShowDialog();

				// You can then add code to navigate the data held in the class.
				// When navigating this object model you should refer to the documentation
				// generated in the directory:
				// C:\Users\Vasily\Documents\Liquid\schemas\schema0.xsd.Output\DocumentationCS\.
				// The help should be compiled into a chm before being used, (use build.bat)
				//- HTML Help Workshop is required to perform this,
				// and can be downloaded from Microsoft. The path to the help compiler (hhc.exe) 
				// may need adjusting in build.bat
				
				// ...

				////////////////////////////////////////////////////////////////////				
				// The Xml can be extracted from the class using one of 3 methods; //
				////////////////////////////////////////////////////////////////////
				
				// This method will extract the xml into a string. The string is always encoded 
				// using Unicode, there a number of options allowing the headers, 
				// end of line & indenting to be set.
				string strXml = elm.ToXml();
				Console.WriteLine(strXml);
				
				// This method will extract the xml into a file. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				elm.ToXmlFile(filename + ".testouput.xml");
				
				// This method will extract the xml into a stream. This method provides options
				// for changing the encoding (UTF-8, UTF-16) as well as headers, 
				// end of line and indenting.
				// This method is useful when a specific encoding is required (typically
				// UTF-8), in order to transmit it over an 8-bit connection, smtp etc
				// without the need to write the xml to file first.
				System.IO.Stream stmXml = elm.ToXmlStream();

			}
			catch (Exception e)
			{
				DisplayError(e);
			}
		}
		#endregion
		#endregion

		#region Error Handler
		private void DisplayError(Exception ex)
		{
			string errText = "Error - \n";
			// Note : exceptions are likely to contain inner exceptions
			// that provide further detail about the error.
			while (ex != null)
			{
				errText += ex.Message + "\n";
				ex = ex.InnerException;
			}
			MessageBox.Show(this,  errText, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); 
		}
		#endregion

		#region Basic Form Plumbing
		#region SampleApp - Constructor
		public SampleApp()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		#endregion 

		#region Dispose
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion

		#region Main
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new SampleApp());
		}
		#endregion 
	
		#region SampleApp_Load - Populates the list box
		private void SampleApp_Load(object sender, System.EventArgs e)
		{
			//lblInfo.Text = "All Sample XML Files that you specified in the Wizard will be shown here, see btnLoad_Click(...) method in SampleApp.cs.\n\nSelect the file you want to load into the simple viewer and click the 'Load Selected XML File' button:";
			lstFiles.Items.AddRange(new object[] {
													"Browse for a File to Load"
													});
            lstFiles.SelectedIndex = 0;
		}
		#endregion

		#region btnClose_Click
		private void btnClose_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
		#endregion
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lstFiles = new System.Windows.Forms.ListBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(344, 96);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(96, 48);
            this.btnLoad.TabIndex = 2;
            this.btnLoad.Text = "Load Selected XML File";
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(344, 216);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(96, 24);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lstFiles
            // 
            this.lstFiles.HorizontalScrollbar = true;
            this.lstFiles.Location = new System.Drawing.Point(8, 96);
            this.lstFiles.Name = "lstFiles";
            this.lstFiles.Size = new System.Drawing.Size(328, 134);
            this.lstFiles.TabIndex = 1;
            this.lstFiles.DoubleClick += new System.EventHandler(this.btnLoad_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "xml";
            this.openFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(291, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "��������� ��� XML";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(140, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(279, 20);
            this.textBox1.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "�������";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(13, 54);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(125, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "��������� ��� CSV";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // SampleApp
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(448, 246);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lstFiles);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnLoad);
            this.Name = "SampleApp";
            this.Text = "Liquid XML Sample";
            this.Load += new System.EventHandler(this.SampleApp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        #endregion

        string filePath;

        private void button1_Click(object sender, EventArgs e)
        {
            List<Persons> prsn = new List<Persons>();

            using (TextReader reader = new StreamReader(filePath, Encoding.GetEncoding(1251)))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.ClassMapping<DataHelperMapResult>();
                csv.Configuration.Delimiter = ";";
                var records = csv.GetRecords<Persons>();

                foreach (var person in records)
                {
                    Persons recResult = new Persons();

                    recResult.PackageID = person.PackageID;
                    recResult.ID = person.ID;
                    recResult.RecType = person.RecType;
                    recResult.OSZCode = person.OSZCode;
                    recResult.SnilsRecip = person.SnilsRecip;
                    recResult.FamilyNameRecip = person.FamilyNameRecip;
                    recResult.NameRecip = person.NameRecip;
                    recResult.PatronymicRecip = person.PatronymicRecip;
                    recResult.GenderRecip = person.GenderRecip;
                    recResult.BirthDateRecip = person.BirthDateRecip;
                    recResult.DocSeriesRecip = person.DocSeriesRecip;
                    recResult.DocNumberRecip = person.DocNumberRecip;
                    recResult.DocIssueDateRecip = person.DocIssueDateRecip;
                    recResult.DocIssuerRecip = person.DocIssuerRecip;
                    recResult.LMSZID = person.LMSZID;
                    recResult.CategoryID = person.CategoryID;
                    recResult.DecisionDate = person.DecisionDate;
                    recResult.DateStart = person.DateStart;
                    recResult.DateFinish = person.DateFinish;
                    recResult.UsingSign = person.UsingSign;
                    recResult.Amount = person.Amount;
                    recResult.LastChanging = person.LastChanging;
                    recResult.SnilsReason = person.SnilsReason;
                    recResult.FamilyNameReason = person.FamilyNameReason;
                    recResult.NameReason = person.NameReason;
                    recResult.PatronymicReason = person.PatronymicReason;
                    recResult.GenderReason = person.GenderReason;
                    recResult.BirthdateReason = person.BirthdateReason;
                    recResult.DoctypeReason = person.DoctypeReason;
                    recResult.DocSeriesReason = person.DocSeriesReason;
                    recResult.DocNumberReason = person.DocNumberReason;
                    recResult.DocIssueDateReason = person.DocIssueDateReason;
                    recResult.DocIssuerReason = person.DocIssuerReason;

                    prsn.Add(recResult);
                }
            }

            List<RecordXML> list = new List<RecordXML>();

            //������ ����������� �� ������� ���������
            List<string> ids = prsn.Select(p => p.ID).Distinct().ToList();
            foreach (string id in ids)
            {
                //����� ������ ������
                List<Persons> item = prsn.Where(p => p.ID == id).Where(s => s.RecType == "fact").ToList();
                List<Persons> prozhivWithOwner = prsn.Where(p => p.ID == id).ToList();
                //���� ��������, �����
                if (item.Count == 1 && prozhivWithOwner.Count == 1)
                {
                    RecordXML odinokiy = new RecordXML();
                    odinokiy.PackageID = item[0].PackageID;
                    odinokiy.ID = item[0].ID;
                    odinokiy.OSZCode = item[0].OSZCode;
                    odinokiy.SnilsRecip = item[0].SnilsRecip;
                    odinokiy.FamilyNameRecip = item[0].FamilyNameRecip;
                    odinokiy.NameRecip = item[0].NameRecip;
                    odinokiy.PatronymicRecip = item[0].PatronymicRecip;
                    odinokiy.GenderRecip = item[0].GenderRecip;
                    odinokiy.BirthDateRecip = item[0].BirthDateRecip;
                    odinokiy.DocSeriesRecip = item[0].DocSeriesRecip;
                    odinokiy.DocNumberRecip = item[0].DocNumberRecip;
                    odinokiy.DocIssueDateRecip = item[0].DocIssueDateRecip;
                    odinokiy.DocIssuerRecip = item[0].DocIssuerRecip;
                    odinokiy.LMSZID = item[0].LMSZID;
                    odinokiy.CategoryID = item[0].CategoryID;
                    odinokiy.DecisionDate = item[0].DecisionDate;
                    odinokiy.DateStart = item[0].DateStart;
                    odinokiy.DateFinish = item[0].DateFinish;
                    odinokiy.UsingSign = item[0].UsingSign;
                    odinokiy.Amount = item[0].Amount;
                    odinokiy.LastChanging = item[0].LastChanging;


                    list.Add(odinokiy);
                }
                //�� ��������
                else if (item.Count == 1 && prozhivWithOwner.Count > 1)
                {
                    Persons rec = item.Where(p => p.RecType == "fact").FirstOrDefault();

                    if (rec != null)
                    {
                        RecordXML neOdinokiy = new RecordXML();
                        neOdinokiy.PackageID = rec.PackageID;
                        neOdinokiy.ID = rec.ID;
                        neOdinokiy.OSZCode = rec.OSZCode;
                        neOdinokiy.SnilsRecip = rec.SnilsRecip;
                        neOdinokiy.FamilyNameRecip = rec.FamilyNameRecip;
                        neOdinokiy.NameRecip = rec.NameRecip;
                        neOdinokiy.PatronymicRecip = rec.PatronymicRecip;
                        neOdinokiy.GenderRecip = rec.GenderRecip;
                        neOdinokiy.BirthDateRecip = rec.BirthDateRecip;
                        neOdinokiy.DocSeriesRecip = rec.DocSeriesRecip;
                        neOdinokiy.DocNumberRecip = rec.DocNumberRecip;
                        neOdinokiy.DocIssueDateRecip = rec.DocIssueDateRecip;
                        neOdinokiy.DocIssuerRecip = rec.DocIssuerRecip;
                        neOdinokiy.LMSZID = rec.LMSZID;
                        neOdinokiy.CategoryID = rec.CategoryID;
                        neOdinokiy.DecisionDate = rec.DecisionDate;
                        neOdinokiy.DateStart = rec.DateStart;
                        neOdinokiy.DateFinish = rec.DateFinish;
                        neOdinokiy.UsingSign = rec.UsingSign;
                        neOdinokiy.Amount = rec.Amount;
                        neOdinokiy.LastChanging = rec.LastChanging;

                        neOdinokiy.prozhiv = new List<Prozhiv>();

                        List<Persons> reasons = prozhivWithOwner.Where(p => p.RecType == "reason").ToList();
                        foreach (Persons reason in reasons)
                        {
                            //public string SnilsReason { get; set; }
                            //public string FamilyNameReason { get; set; }
                            //public string NameReason { get; set; }
                            //public string PatronymicReason { get; set; }
                            //public string GenderReason { get; set; }
                            //public DateTime? BirthdateReason { get; set; }
                            //public string DoctypeReason { get; set; }
                            //public string DocSeriesReason { get; set; }
                            //public string DocNumberReason { get; set; }
                            //public DateTime? DocIssueDateReason { get; set; }
                            //public string DocIssuerReason { get; set; }
        //����������� ����� ���� ��� � ���������, ��� � � ��������������
        Prozhiv prozhiv = new Prozhiv();
                            
                            prozhiv.SnilsReason = reason.SnilsReason;
                            prozhiv.FamilyNameReason = reason.FamilyNameReason;
                            prozhiv.NameReason = reason.NameReason;
                            prozhiv.PatronymicReason = reason.PatronymicReason;
                            prozhiv.GenderReason = reason.GenderReason;
                            prozhiv.BirthdateReason = reason.BirthdateReason;
                            prozhiv.DoctypeReason = reason.DoctypeReason;
                            prozhiv.DocSeriesReason = reason.DocSeriesReason;
                            prozhiv.DocNumberReason = reason.DocNumberReason;
                            prozhiv.DocIssueDateReason = reason.DocIssueDateReason;
                            prozhiv.DocIssuerReason = reason.DocIssuerReason;


                            neOdinokiy.prozhiv.Add(prozhiv);

                        }
                        list.Add(neOdinokiy);
                    }
                }

            }

            var packID = list.Select(p=> p.PackageID).FirstOrDefault();
            //List<RecordXML> recordsXml = new GetDataOFL().GetXMLRecords(records);
            ns55.Data rt = new ns55.Data();
            rt.Package = new schema0Lib.Package();
            //rt.Package.PackageID = Guid.NewGuid().ToString();
            rt.Package.PackageID = packID;
            rt.Package.Elements = new ns51.Elements();

            List<ns51.Fact> facts = new List<ns51.Fact>();
            foreach (RecordXML rec in list)
            {
                string gender = "";
                if (rec.GenderRecip == "�")
                {
                    gender = "Female";
                }
                else
                {
                    gender = "Male";
                }

                ns51.Fact fact = new ns51.Fact();
                fact.ID = rec.ID;

                fact.OSZCode = rec.OSZCode;
                fact.MSZ_receiver = new schema0Lib.MSZ_receiver();
                fact.MSZ_receiver.SNILS = rec.SnilsRecip;
                fact.MSZ_receiver.FamilyName = rec.FamilyNameRecip;
                fact.MSZ_receiver.FirstName = rec.NameRecip;
                if (rec.PatronymicRecip != "")
                    fact.MSZ_receiver.Patronymic = rec.PatronymicRecip;
                else
                    //������ ������� ���� ��� ��������
                    fact.MSZ_receiver.Patronymic = "-";



                fact.MSZ_receiver.Gender = gender;
                fact.MSZ_receiver.BirthDate = new XmlDateTime((DateTime)rec.BirthDateRecip);
                fact.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();

                fact.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
                fact.MSZ_receiver.IdentityDoc.PassportRF.Series = rec.DocSeriesRecip;
                fact.MSZ_receiver.IdentityDoc.PassportRF.Number = rec.DocNumberRecip;
                fact.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime((DateTime)rec.DocIssueDateRecip);
                fact.MSZ_receiver.IdentityDoc.PassportRF.Issuer = rec.DocIssuerRecip;
                //���� ����, �� �� ������ Reason_persons
                if (rec.prozhiv != null && rec.prozhiv.Count >= 1)
                {
                    fact.Reason_persons = new schema0Lib.Reason_persons();

                    foreach (Prozhiv prozhiv in rec.prozhiv)
                    {
                        //���������� ����������� �� �����


                        schema0Lib.PrsnInfo prsnInfo = new schema0Lib.PrsnInfo();

                        prsnInfo.SNILS = prozhiv.SnilsReason;
                        prsnInfo.FamilyName = prozhiv.FamilyNameReason;
                        prsnInfo.FirstName = prozhiv.NameReason;
                        if (prozhiv.PatronymicReason != "")
                            prsnInfo.Patronymic = prozhiv.PatronymicReason;



                        if (prozhiv.GenderReason == "�")
                        {
                            gender = "Female";
                        }
                        else
                        {
                            gender = "Male";
                        }
                        prsnInfo.Gender = gender;
                        prsnInfo.BirthDate = new XmlDateTime((DateTime)prozhiv.BirthdateReason);
                        prsnInfo.IdentityDoc = new ns53.IdentityDocA();
                        //���� �������, ������� ��� ���� ������ �������� � ������������� � ��������
                        if (prozhiv.DoctypeReason == "BCert")
                        {

                            prsnInfo.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
                            prsnInfo.IdentityDoc.PassportRF.Series = prozhiv.DocSeriesReason;
                            prsnInfo.IdentityDoc.PassportRF.Number = prozhiv.DocNumberReason;
                            prsnInfo.IdentityDoc.PassportRF.IssueDate = new XmlDateTime((DateTime)prozhiv.DocIssueDateReason);
                            prsnInfo.IdentityDoc.PassportRF.Issuer = prozhiv.DocIssuerReason;
                        }
                        else if (prozhiv.DoctypeReason == "PassRF")
                        {
                            prsnInfo.IdentityDoc.BirthCertificate = new schema0Lib.BirthCertificate();
                            prsnInfo.IdentityDoc.BirthCertificate.Series = prozhiv.DocSeriesReason;
                            prsnInfo.IdentityDoc.BirthCertificate.Number = prozhiv.DocNumberReason;
                            prsnInfo.IdentityDoc.BirthCertificate.IssueDate = new XmlDateTime((DateTime)prozhiv.DocIssueDateReason);
                            prsnInfo.IdentityDoc.BirthCertificate.Issuer = prozhiv.DocIssuerReason;
                        }
                        fact.Reason_persons.PrsnInfo.Add(prsnInfo);
                    }


                }

                fact.LMSZID = rec.LMSZID;
                fact.CategoryID = rec.CategoryID;
                fact.Decision_date = new XmlDateTime((DateTime)rec.DecisionDate);
                fact.DateStart = new XmlDateTime((DateTime)rec.DateStart);
                fact.DateFinish = new XmlDateTime((DateTime)rec.DateFinish);
                fact.NeedsCriteria = new schema0Lib.NeedsCriteria();
                fact.NeedsCriteria.UsingSign = false;
                fact.NeedsCriteria.Criteria = "";
                fact.Assignment_info = new schema0Lib.Assignment_info();
                //   fact.Assignment_info.ServiceForm = new ns52.ServiceForm();
                fact.Assignment_info.Monetary_form = new ns52.Monetary_form();
                fact.Assignment_info.Monetary_form.Amount = rec.Amount.ToString();
                fact.LastChanging = new XmlDateTime(DateTime.Now);

                rt.Package.Elements.Fact.Add(fact);
            }

            if (rt.Package.Elements.Fact.Count == 0)
            {
                MessageBox.Show("��� ������� ��� ��������");
                return;
            }
            ////������ �������� ���
            //Dictionary<int, string> monthes = new GetDataOFL().Monthes();
            //using (TextWriter writer = new StreamWriter("������_��������_���.csv", false, Encoding.GetEncoding(1251)))
            //{

            //    var csv = new CsvWriter(writer);
            //    csv.Configuration.Delimiter = ";";
            //    csv.WriteRecords(GetData.errorList); // where values implements IEnumerable
            //}


            rt.ToXmlFile("test.xml");
            MessageBox.Show("test.xml ���� ������� ���������");

            //ns55.Data rt = new ns55.Data();
            //rt.Package = new schema0Lib.Package();
            //rt.Package.PackageID = "ba028da4-1432-446d-82f9-a563d69a0c0c";
            //rt.Package.Elements = new ns51.Elements();
            //ns51.Fact fact = new ns51.Fact();
            //rt.Package.Elements.Fact.Add(fact);
            //fact.ID = "7b66dade-ef8e-4423-a57e-58a07b7b7908";
            //fact.OSZCode = "0336.000001";
            //fact.MSZ_receiver = new schema0Lib.MSZ_receiver();
            //fact.MSZ_receiver.SNILS = "03926714976";
            //fact.MSZ_receiver.FamilyName = "����������";
            //fact.MSZ_receiver.FirstName = "����";
            //fact.MSZ_receiver.Patronymic = "��������������";
            //fact.MSZ_receiver.Gender = "Male";
            //fact.MSZ_receiver.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();
            //fact.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //fact.MSZ_receiver.IdentityDoc.PassportRF.Series = "9811";
            //fact.MSZ_receiver.IdentityDoc.PassportRF.Number = "387425";
            //fact.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.MSZ_receiver.IdentityDoc.PassportRF.Issuer = "�� ���� ������ �� ��(�) � �������� �����";
            //fact.Reason_persons = new schema0Lib.Reason_persons();
            //schema0Lib.PrsnInfo prsnInfo = new schema0Lib.PrsnInfo();
            //fact.Reason_persons.PrsnInfo.Add(prsnInfo);
            //prsnInfo.SNILS = "16391907086";
            //prsnInfo.FamilyName = "����������";
            //prsnInfo.FirstName = "�����";
            //prsnInfo.Patronymic = "�����";
            //prsnInfo.Gender = "Male";
            //prsnInfo.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo.IdentityDoc = new ns53.IdentityDocA();
            //prsnInfo.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //prsnInfo.IdentityDoc.BirthCertificate = new schema0Lib.BirthCertificate();
            //prsnInfo.IdentityDoc.BirthCertificate.Series = "I-��";
            //prsnInfo.IdentityDoc.BirthCertificate.Number = "775463";
            //prsnInfo.IdentityDoc.BirthCertificate.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo.IdentityDoc.BirthCertificate.Issuer = "����� ���������� ���� ��� �������������";
            //schema0Lib.PrsnInfo prsnInfo1 = new schema0Lib.PrsnInfo();
            //fact.Reason_persons.PrsnInfo.Add(prsnInfo1);
            //prsnInfo1.SNILS = "16391907288";
            //prsnInfo1.FamilyName = "����������";
            //prsnInfo1.FirstName = "���������";
            //prsnInfo1.Patronymic = "�����";
            //prsnInfo1.Gender = "Male";
            //prsnInfo1.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo1.IdentityDoc = new ns53.IdentityDocA();
            //prsnInfo1.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //prsnInfo1.IdentityDoc.BirthCertificate = new schema0Lib.BirthCertificate();
            //prsnInfo1.IdentityDoc.BirthCertificate.Series = "I-��";
            //prsnInfo1.IdentityDoc.BirthCertificate.Number = "706338";
            //prsnInfo1.IdentityDoc.BirthCertificate.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo1.IdentityDoc.BirthCertificate.Issuer = "����� ���������� ���� ��� �������������";
            //schema0Lib.PrsnInfo prsnInfo2 = new schema0Lib.PrsnInfo();
            //fact.Reason_persons.PrsnInfo.Add(prsnInfo2);
            //prsnInfo2.SNILS = "11282760337";
            //prsnInfo2.FamilyName = "�����������";
            //prsnInfo2.FirstName = "����";
            //prsnInfo2.Patronymic = "��������";
            //prsnInfo2.Gender = "Female";
            //prsnInfo2.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo2.IdentityDoc = new ns53.IdentityDocA();
            //prsnInfo2.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //prsnInfo2.IdentityDoc.PassportRF.Series = "9803";
            //prsnInfo2.IdentityDoc.PassportRF.Number = "749959";
            //prsnInfo2.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo2.IdentityDoc.PassportRF.Issuer = "��� ��������� �-�� ��(�)";
            //prsnInfo2.IdentityDoc.BirthCertificate = new schema0Lib.BirthCertificate();
            //fact.LMSZID = "5bd25f52-3f72-420d-b82b-df7fd796c71e";
            //fact.CategoryID = "46277ffb-fc85-42ed-ab76-23e059f26317";
            //fact.Decision_date = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.DateStart = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.DateFinish = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.NeedsCriteria = new schema0Lib.NeedsCriteria();
            //fact.NeedsCriteria.UsingSign = false;
            //fact.NeedsCriteria.Criteria = "";
            //fact.Assignment_info = new schema0Lib.Assignment_info();
            //fact.Assignment_info.ServiceForm = new ns52.ServiceForm();
            //fact.Assignment_info.Monetary_form = new ns52.Monetary_form();
            //fact.Assignment_info.Monetary_form.Amount = "2649.50";
            //fact.LastChanging = new XmlDateTime(new DateTime(2019, 7, 16));
            //ns51.Fact fact1 = new ns51.Fact();
            //rt.Package.Elements.Fact.Add(fact1);
            //fact1.ID = "08180269-3617-426c-8fdc-d9c5485c46e0";
            //fact1.OSZCode = "0336.000001";
            //fact1.MSZ_receiver = new schema0Lib.MSZ_receiver();
            //fact1.MSZ_receiver.SNILS = "08779192841";
            //fact1.MSZ_receiver.FamilyName = "���������";
            //fact1.MSZ_receiver.FirstName = "���������";
            //fact1.MSZ_receiver.Patronymic = "��������";
            //fact1.MSZ_receiver.Gender = "Female";
            //fact1.MSZ_receiver.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();
            //fact1.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.Series = "9815";
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.Number = "630681";
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.Issuer = "��� ���� ������ �� ��(�) � �. �������";
            //fact1.Reason_persons = new schema0Lib.Reason_persons();
            //fact1.LMSZID = "5bd25f52-3f72-420d-b82b-df7fd796c71e";
            //fact1.CategoryID = "46277ffb-fc85-42ed-ab76-23e059f26317";
            //fact1.Decision_date = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.DateStart = new XmlDateTime(new DateTime(2019, 7, 1));
            //fact1.DateFinish = new XmlDateTime(new DateTime(2019, 12, 31));
            //fact1.NeedsCriteria = new schema0Lib.NeedsCriteria();
            //fact1.NeedsCriteria.UsingSign = false;
            //fact1.NeedsCriteria.Criteria = "";
            //fact1.Assignment_info = new schema0Lib.Assignment_info();
            //fact1.Assignment_info.ServiceForm = new ns52.ServiceForm();
            //fact1.Assignment_info.Monetary_form = new ns52.Monetary_form();
            //fact1.Assignment_info.Monetary_form.Amount = "2012.63";
            //fact1.LastChanging = new XmlDateTime(new DateTime(2019, 7, 16, 10, 54, 15));

            //ns55.Data rt = new ns55.Data();
            //rt.Package = new schema0Lib.Package();
            //rt.Package.PackageID = "7deea631-8d9d-46d6-83fe-fd88cda3267a";
            //rt.Package.Elements = new ns51.Elements();
            //ns51.Fact fact = new ns51.Fact();
            //rt.Package.Elements.Fact.Add(fact);
            //fact.ID = "0bdea799-f6ca-4abf-bb62-4cc60f26e012";
            //fact.OSZCode = "2472.000001";
            //fact.MSZ_receiver = new schema0Lib.MSZ_receiver();
            //fact.MSZ_receiver.SNILS = "11111111111";
            //fact.MSZ_receiver.FamilyName = "����������";
            //fact.MSZ_receiver.FirstName = "����";
            //fact.MSZ_receiver.Patronymic = "����������";
            //fact.MSZ_receiver.Gender = "Female";
            //fact.MSZ_receiver.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();
            //fact.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //fact.MSZ_receiver.IdentityDoc.PassportRF.Series = "4305";
            //fact.MSZ_receiver.IdentityDoc.PassportRF.Number = "335638";
            //fact.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.MSZ_receiver.IdentityDoc.PassportRF.Issuer = "��������������� �-� ����������� ��� ��";

            //fact.LMSZID = "4a382da4-6075-4500-8671-fc327c640275";
            //fact.CategoryID = "efd64fa2-cda9-45a7-90a1-bcfd600a03ff";
            //fact.Decision_date = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.DateStart = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.DateFinish = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact.NeedsCriteria = new schema0Lib.NeedsCriteria();
            //fact.NeedsCriteria.UsingSign = false;
            //fact.NeedsCriteria.Criteria = "";
            //fact.Assignment_info = new schema0Lib.Assignment_info();

            //fact.Assignment_info.Monetary_form = new ns52.Monetary_form();
            //fact.Assignment_info.Monetary_form.Amount = "81,14";
            //fact.LastChanging = new XmlDateTime(new DateTime(2019, 7, 16, 10, 54, 15));
            //ns51.Fact fact1 = new ns51.Fact();
            //rt.Package.Elements.Fact.Add(fact1);
            //fact1.ID = "e6dc8a5a-469b-479a-9c93-209c83f35bd4";
            //fact1.OSZCode = "2472.000001";
            //fact1.MSZ_receiver = new schema0Lib.MSZ_receiver();
            //fact1.MSZ_receiver.SNILS = "22222222222";
            //fact1.MSZ_receiver.FamilyName = "�����������";
            //fact1.MSZ_receiver.FirstName = "�������";
            //fact1.MSZ_receiver.Patronymic = "��������";
            //fact1.MSZ_receiver.Gender = "Female";
            //fact1.MSZ_receiver.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();
            //fact1.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.Series = "4656";
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.Number = "125979";
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.MSZ_receiver.IdentityDoc.PassportRF.Issuer = "�� � �.���������� ����� ������ �� ���������� ���. � ���������� ������";

            //fact1.LMSZID = "4a382da4-6075-4500-8671-fc327c640275";
            //fact1.CategoryID = "efd64fa2-cda9-45a7-90a1-bcfd600a03ff";
            //fact1.Decision_date = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.DateStart = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.DateFinish = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact1.NeedsCriteria = new schema0Lib.NeedsCriteria();
            //fact1.NeedsCriteria.UsingSign = false;
            //fact1.NeedsCriteria.Criteria = "";
            //fact1.Assignment_info = new schema0Lib.Assignment_info();

            //fact1.Assignment_info.Monetary_form = new ns52.Monetary_form();
            //fact1.Assignment_info.Monetary_form.Amount = "1045,58";
            //fact1.LastChanging = new XmlDateTime(new DateTime(2019, 7, 16, 10, 54, 15));
            //ns51.Fact fact2 = new ns51.Fact();
            //rt.Package.Elements.Fact.Add(fact2);
            //fact2.ID = "8f4d3384-99f3-4f16-befb-7190c065e9af";
            //fact2.OSZCode = "2472.000001";
            //fact2.MSZ_receiver = new schema0Lib.MSZ_receiver();
            //fact2.MSZ_receiver.SNILS = "33333333333";
            //fact2.MSZ_receiver.FamilyName = "������";
            //fact2.MSZ_receiver.FirstName = "�����";
            //fact2.MSZ_receiver.Patronymic = "����������";
            //fact2.MSZ_receiver.Gender = "Male";
            //fact2.MSZ_receiver.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact2.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();
            //fact2.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //fact2.MSZ_receiver.IdentityDoc.PassportRF.Series = "4471";
            //fact2.MSZ_receiver.IdentityDoc.PassportRF.Number = "654897";
            //fact2.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact2.MSZ_receiver.IdentityDoc.PassportRF.Issuer = "��������������� �-� ����������� ��� ��";
            //fact2.Reason_persons = new schema0Lib.Reason_persons();
            //schema0Lib.PrsnInfo prsnInfo = new schema0Lib.PrsnInfo();

            //fact2.Reason_persons.PrsnInfo.Add(prsnInfo);
            //prsnInfo.SNILS = "77777777777";
            //prsnInfo.FamilyName = "����������";
            //prsnInfo.FirstName = "�������";
            //prsnInfo.Patronymic = "�����������";
            //prsnInfo.Gender = "Female";
            //prsnInfo.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo.IdentityDoc = new ns53.IdentityDocA();

            //prsnInfo.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //prsnInfo.IdentityDoc.PassportRF.Series = "4225";
            //prsnInfo.IdentityDoc.PassportRF.Number = "335528";
            //prsnInfo.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo.IdentityDoc.PassportRF.Issuer = "��������������� �-� ����������� ��� ��";

            //schema0Lib.PrsnInfo prsnInfo1 = new schema0Lib.PrsnInfo();
            //fact2.Reason_persons.PrsnInfo.Add(prsnInfo);
            //prsnInfo1.SNILS = "77777777777";
            //prsnInfo1.FamilyName = "����������";
            //prsnInfo1.FirstName = "�������";
            //prsnInfo1.Patronymic = "�����������";
            //prsnInfo1.Gender = "Female";
            //prsnInfo1.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo1.IdentityDoc = new ns53.IdentityDocA();

            //prsnInfo1.IdentityDoc.BirthCertificate = new schema0Lib.BirthCertificate();
            //prsnInfo1.IdentityDoc.BirthCertificate.Series = "III-��";
            //prsnInfo1.IdentityDoc.BirthCertificate.Number = "335528";
            //prsnInfo1.IdentityDoc.BirthCertificate.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo1.IdentityDoc.BirthCertificate.Issuer = "��������������� �-� ����������� ��� ��";

            //schema0Lib.PrsnInfo prsnInfo2 = new schema0Lib.PrsnInfo();
            //fact2.Reason_persons.PrsnInfo.Add(prsnInfo);
            //prsnInfo2.SNILS = "77777777777";
            //prsnInfo2.FamilyName = "����������";
            //prsnInfo2.FirstName = "�������";
            //prsnInfo2.Patronymic = "�����������";
            //prsnInfo2.Gender = "Female";
            //prsnInfo2.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo2.IdentityDoc = new ns53.IdentityDocA();
            //prsnInfo2.IdentityDoc.BirthCertificate = new schema0Lib.BirthCertificate();
            //prsnInfo2.IdentityDoc.BirthCertificate.Series = "III-��";
            //prsnInfo2.IdentityDoc.BirthCertificate.Number = "335528";
            //prsnInfo2.IdentityDoc.BirthCertificate.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //prsnInfo2.IdentityDoc.BirthCertificate.Issuer = "��������������� �-� ����������� ��� ��";


            //fact2.LMSZID = "4a382da4-6075-4500-8671-fc327c640275";
            //fact2.CategoryID = "efd64fa2-cda9-45a7-90a1-bcfd600a03ff";
            //fact2.Decision_date = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact2.DateStart = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact2.DateFinish = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact2.NeedsCriteria = new schema0Lib.NeedsCriteria();
            //fact2.NeedsCriteria.UsingSign = false;
            //fact2.NeedsCriteria.Criteria = "";
            //fact2.Assignment_info = new schema0Lib.Assignment_info();
            //fact2.Assignment_info.ServiceForm = new ns52.ServiceForm();
            //fact2.Assignment_info.ServiceForm.Amount = "10";
            //fact2.Assignment_info.ServiceForm.MeasuryCode = "03";
            //fact2.Assignment_info.ServiceForm.Content = "����������";
            //fact2.Assignment_info.ServiceForm.Comment = "�����������";
            //fact2.Assignment_info.ServiceForm.EquivalentAmount = 982;
            ////       fact2.Assignment_info.Monetary_form = new ns52.Monetary_form();
            //fact2.LastChanging = new XmlDateTime(new DateTime(2019, 7, 16, 10, 54, 15));
            //ns51.Fact fact3 = new ns51.Fact();
            //rt.Package.Elements.Fact.Add(fact3);
            //fact3.ID = "cf69db17-6fd7-44aa-ab43-e808fcf98ac4";
            //fact3.OSZCode = "2472.000001";
            //fact3.MSZ_receiver = new schema0Lib.MSZ_receiver();
            //fact3.MSZ_receiver.SNILS = "44444444444";
            //fact3.MSZ_receiver.FamilyName = "������������";
            //fact3.MSZ_receiver.FirstName = "������";
            //fact3.MSZ_receiver.Patronymic = "����������";
            //fact3.MSZ_receiver.Gender = "Female";
            //fact3.MSZ_receiver.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact3.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();
            //fact3.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //fact3.MSZ_receiver.IdentityDoc.PassportRF.Series = "4622";
            //fact3.MSZ_receiver.IdentityDoc.PassportRF.Number = "672243";
            //fact3.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact3.MSZ_receiver.IdentityDoc.PassportRF.Issuer = "��������������� �-� ����������� ��� ��";

            //fact3.LMSZID = "4a382da4-6075-4500-8671-fc327c640275";
            //fact3.CategoryID = "efd64fa2-cda9-45a7-90a1-bcfd600a03ff";
            //fact3.Decision_date = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact3.DateStart = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact3.DateFinish = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact3.NeedsCriteria = new schema0Lib.NeedsCriteria();
            //fact3.NeedsCriteria.UsingSign = false;
            //fact3.NeedsCriteria.Criteria = "";
            //fact3.Assignment_info = new schema0Lib.Assignment_info();

            //fact3.Assignment_info.Monetary_form = new ns52.Monetary_form();
            //fact3.Assignment_info.Monetary_form.Amount = "822,45";
            //fact3.LastChanging = new XmlDateTime(new DateTime(2019, 7, 16, 10, 54, 15));
            //ns51.Fact fact4 = new ns51.Fact();
            //rt.Package.Elements.Fact.Add(fact4);
            //fact4.ID = "4a74b00d-666b-4907-b86c-52e1b6f29109";
            //fact4.OSZCode = "2472.000001";
            //fact4.MSZ_receiver = new schema0Lib.MSZ_receiver();
            //fact4.MSZ_receiver.SNILS = "55555555555";
            //fact4.MSZ_receiver.FamilyName = "���������";
            //fact4.MSZ_receiver.FirstName = "�������";
            //fact4.MSZ_receiver.Patronymic = "���������";
            //fact4.MSZ_receiver.Gender = "Male";
            //fact4.MSZ_receiver.BirthDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact4.MSZ_receiver.IdentityDoc = new schema0Lib.IdentityDoc();
            //fact4.MSZ_receiver.IdentityDoc.PassportRF = new schema0Lib.PassportRF();
            //fact4.MSZ_receiver.IdentityDoc.PassportRF.Series = "4654";
            //fact4.MSZ_receiver.IdentityDoc.PassportRF.Number = "673332";
            //fact4.MSZ_receiver.IdentityDoc.PassportRF.IssueDate = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact4.MSZ_receiver.IdentityDoc.PassportRF.Issuer = "��������������� �-� ����������� ��� ��";

            //fact4.LMSZID = "4a382da4-6075-4500-8671-fc327c640275";
            //fact4.CategoryID = "efd64fa2-cda9-45a7-90a1-bcfd600a03ff";
            //fact4.Decision_date = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact4.DateStart = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact4.DateFinish = new XmlDateTime(new DateTime(2019, 7, 16));
            //fact4.NeedsCriteria = new schema0Lib.NeedsCriteria();
            //fact4.NeedsCriteria.UsingSign = false;
            //fact4.NeedsCriteria.Criteria = "";
            //fact4.Assignment_info = new schema0Lib.Assignment_info();

            //fact4.Assignment_info.Monetary_form = new ns52.Monetary_form();
            //fact4.Assignment_info.Monetary_form.Amount = "1667,55";
            //fact4.LastChanging = new XmlDateTime(new DateTime(2019, 7, 16, 10, 54, 15));

            //rt.ToXmlFile("1.xml");


        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFile = new OpenFileDialog())
            {
                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFile.FileName;

                    textBox1.Text = filePath;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            XDocument xDoc = XDocument.Load(filePath);
            XNamespace ns51 = XNamespace.Get("urn://egisso-ru/types/package-RAF/1.0.3");
            XNamespace ns52 = XNamespace.Get("urn://egisso-ru/types/assignment-fact/1.0.3");
            XNamespace ns33 = XNamespace.Get("urn://x-artefacts-smev-gov-ru/supplementary/commons/1.0.1");
            XNamespace ns55 = XNamespace.Get("urn://egisso-ru/msg/10.06.S/1.0.2");
            XNamespace ns54 = XNamespace.Get("urn://egisso-ru/types/basic/1.0.4");
            XNamespace ns53 = XNamespace.Get("urn://egisso-ru/types/prsn-info/1.0.3");
            ;
            List<Persons> pers = new List<Persons>();

            foreach (XElement el in xDoc.Element(ns55 + "data").Element(ns51 + "package").Element(ns51 + "elements").Elements(ns51 + "fact"))
            {
                var item = new Persons();
                //List<Prozhiv> prozh = new List<Prozhiv>();

                item.PackageID = xDoc.Element(ns55 + "data").Element(ns51 + "package").Element(ns51 + "packageID").Value;
                item.ID = el.Element(ns52 + "ID").Value;
                item.RecType = "fact";
                item.OSZCode = el.Element(ns52 + "OSZCode").Value;
                item.SnilsRecip = el.Element(ns52 + "MSZ_receiver").Element(ns53 + "SNILS").Value;
                item.FamilyNameRecip = el.Element(ns52 + "MSZ_receiver").Element(ns33 + "FamilyName").Value;
                item.NameRecip = el.Element(ns52 + "MSZ_receiver").Element(ns33 + "FirstName").Value;
                item.PatronymicRecip = el.Element(ns52 + "MSZ_receiver").Element(ns33 + "Patronymic").Value;
                item.GenderRecip = el.Element(ns52 + "MSZ_receiver").Element(ns53 + "Gender").Value;
                item.BirthDateRecip = DateTime.Parse((el.Element(ns52 + "MSZ_receiver").Element(ns53 + "BirthDate").Value));
                item.DocSeriesRecip = el.Element(ns52 + "MSZ_receiver").Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "Series").Value;
                item.DocNumberRecip = el.Element(ns52 + "MSZ_receiver").Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "Number").Value;
                item.DocIssueDateRecip = DateTime.Parse(el.Element(ns52 + "MSZ_receiver").Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "IssueDate").Value);
                item.DocIssuerRecip = el.Element(ns52 + "MSZ_receiver").Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "Issuer").Value;
                item.LMSZID = el.Element(ns52 + "LMSZID").Value;
                item.CategoryID = el.Element(ns52 + "categoryID").Value;
                item.DecisionDate = DateTime.Parse(el.Element(ns52 + "decision_date").Value);
                item.DateStart = DateTime.Parse(el.Element(ns52 + "dateStart").Value);
                item.DateFinish = DateTime.Parse(el.Element(ns52 + "dateFinish").Value);
                item.UsingSign = el.Element(ns52 + "needsCriteria").Element(ns52 + "usingSign").Value;
                item.Amount = el.Element(ns52 + "assignment_info").Element(ns52 + "monetary_form").Element(ns52 + "amount").Value;
                item.LastChanging = el.Element(ns51 + "lastChanging").Value;

                pers.Add(item);
                try
                {
                    foreach (XElement el2 in el.Element(ns52 + "reason_persons").Elements(ns53 + "prsnInfo"))
                    {

                        var item2 = new Persons();
                        item2.ID = el.Element(ns52 + "ID").Value;
                        item2.RecType = "reason";
                        item2.SnilsReason = el2.Element(ns53 + "SNILS").Value;
                        item2.FamilyNameReason = el2.Element(ns33 + "FamilyName").Value;
                        item2.NameReason = el2.Element(ns33 + "FirstName").Value;
                        item2.PatronymicReason = el2.Element(ns33 + "Patronymic").Value;
                        item2.GenderReason = el2.Element(ns53 + "Gender").Value;
                        item2.BirthdateReason = DateTime.Parse(el2.Element(ns53 + "BirthDate").Value);
                        if (el2.Element(ns53 + "IdentityDoc").Element(ns54 + "BirthCertificate") != null)
                        {
                            item2.DoctypeReason = "BCert";
                            item2.DocSeriesReason = el2.Element(ns53 + "IdentityDoc").Element(ns54 + "BirthCertificate").Element(ns54 + "Series").Value;
                            item2.DocNumberReason = el2.Element(ns53 + "IdentityDoc").Element(ns54 + "BirthCertificate").Element(ns54 + "Number").Value;
                            item2.DocIssueDateReason = DateTime.Parse(el2.Element(ns53 + "IdentityDoc").Element(ns54 + "BirthCertificate").Element(ns54 + "IssueDate").Value);
                            item2.DocIssuerReason = el2.Element(ns53 + "IdentityDoc").Element(ns54 + "BirthCertificate").Element(ns54 + "Issuer").Value;
                        }
                        else
                        {
                            item2.DoctypeReason = "PassRF";
                            item2.DocSeriesReason = el2.Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "Series").Value;
                            item2.DocNumberReason = el2.Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "Number").Value;
                            item2.DocIssueDateReason = DateTime.Parse(el2.Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "IssueDate").Value);
                            item2.DocIssuerReason = el2.Element(ns53 + "IdentityDoc").Element(ns54 + "PassportRF").Element(ns54 + "Issuer").Value;
                        }

                        //Console.WriteLine(el2.Element(ns33 + "FamilyName").Value + " " + el2.Element(ns33 + "FirstName").Value + " ");

                        pers.Add(item2);
                        string werew = "asdw";

                    }
                }
                catch (Exception ex)
                {
                    int yy = 0;
                    yy++;

                }
            }

            using (var writer = new StreamWriter("test.csv", false, Encoding.GetEncoding(1251)))
            using (var csv = new CsvWriter(writer))
            {
                csv.Configuration.Delimiter = ";";
                csv.WriteRecords(pers);
                MessageBox.Show("������");
            }
        }
    }
}

