﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleApp
{
    public class Persons
    {
        public string PackageID { get; set; }
        public string ID { get; set; }
        public string RecType { get; set; }
        public string OSZCode { get; set; }


        public string SnilsRecip { get; set; }
        public string FamilyNameRecip { get; set; }
        public string NameRecip { get; set; }
        public string PatronymicRecip { get; set; }
        public string GenderRecip { get; set; }
        public DateTime? BirthDateRecip { get; set; }
        //public string BirthDateRecip { get; set; }

        public string DocSeriesRecip { get; set; }
        public string DocNumberRecip { get; set; }
        public DateTime? DocIssueDateRecip { get; set; }
        //public string DocIssueDateRecip { get; set; }
        public string DocIssuerRecip { get; set; }

        public string LMSZID { get; set; }
        public string CategoryID { get; set; }
        public DateTime? DecisionDate { get; set; }
        //public string DecisionDate { get; set; }
        public DateTime? DateStart { get; set; }
        //public string DateStart { get; set; }
        public DateTime? DateFinish { get; set; }
        //public string DateFinish { get; set; }
        public string UsingSign { get; set; }


        //public decimal? Amount { get; set; }
        public string Amount { get; set; }
        //public DateTime? LastChanging { get; set; }
        public string LastChanging { get; set; }


        public string SnilsReason { get; set; }
        public string FamilyNameReason { get; set; }
        public string NameReason { get; set; }
        public string PatronymicReason { get; set; }
        public string GenderReason { get; set; }
        public DateTime? BirthdateReason { get; set; }
        //public string BirthdateReason { get; set; }

        public string DoctypeReason { get; set; }
        public string DocSeriesReason { get; set; }
        public string DocNumberReason { get; set; }
        public DateTime? DocIssueDateReason { get; set; }
        //public string DocIssueDateReason { get; set; }
        public string DocIssuerReason { get; set; }
    }

    public class RecordXML : Persons
    {

        public List<Prozhiv> prozhiv;

    }

    public class Prozhiv
    {
        public string SnilsReason { get; set; }
        public string FamilyNameReason { get; set; }
        public string NameReason { get; set; }
        public string PatronymicReason { get; set; }
        public string GenderReason { get; set; }
        public DateTime? BirthdateReason { get; set; }
        public string DoctypeReason { get; set; }
        public string DocSeriesReason { get; set; }
        public string DocNumberReason { get; set; }
        public DateTime? DocIssueDateReason { get; set; }
        public string DocIssuerReason { get; set; }

    }
}
