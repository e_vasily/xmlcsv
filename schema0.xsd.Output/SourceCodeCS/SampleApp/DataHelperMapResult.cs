﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CsvHelper.Configuration;

namespace SampleApp
{
    public sealed class DataHelperMapResult : CsvClassMap<Persons>
    {
        public DataHelperMapResult() 
        {
            Map(m => m.PackageID).Index(0);
            Map(m => m.ID).Index(1);
            Map(m => m.RecType).Index(2);
            Map(m => m.OSZCode).Index(3);
            Map(m => m.SnilsRecip).Index(4);
            Map(m => m.FamilyNameRecip).Index(5);
            Map(m => m.NameRecip).Index(6);
            Map(m => m.PatronymicRecip).Index(7);
            Map(m => m.GenderRecip).Index(8);
            Map(m => m.BirthDateRecip).Index(9);
            Map(m => m.DocSeriesRecip).Index(10);
            Map(m => m.DocNumberRecip).Index(11);
            Map(m => m.DocIssueDateRecip).Index(12);
            Map(m => m.DocIssuerRecip).Index(13);
            Map(m => m.LMSZID).Index(14);
            Map(m => m.CategoryID).Index(15);
            Map(m => m.DecisionDate).Index(16);
            Map(m => m.DateStart).Index(17);
            Map(m => m.DateFinish).Index(18);
            Map(m => m.UsingSign).Index(19);
            Map(m => m.Amount).Index(20);
            Map(m => m.LastChanging).Index(21);

            Map(m => m.SnilsReason).Index(22);
            Map(m => m.FamilyNameReason).Index(23);
            Map(m => m.NameReason).Index(24);
            Map(m => m.PatronymicReason).Index(25);
            Map(m => m.GenderReason).Index(26);
            Map(m => m.BirthdateReason).Index(27);
            Map(m => m.DoctypeReason).Index(28);
            Map(m => m.DocSeriesReason).Index(29);
            Map(m => m.DocNumberReason).Index(30);
            Map(m => m.DocIssueDateReason).Index(31);
            Map(m => m.DocIssuerReason).Index(32);
        }
    }
}
